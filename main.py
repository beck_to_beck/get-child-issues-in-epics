from library.api_calls import ApiCalls
import csv

def main():
    # Authentication
    instance = 'https://<instance>.atlassian.net/'
    username = '<e-mail>'
    password = '<api_token>'

    # Set Authentication
    api_call = ApiCalls()
    api_call.setAuth('basic', username=username, password=password)

    # jql quqery
    jql = "project in (ABC, DEF, GHI) and issuetype = Epic"

    # Fields wanted
    fields = ['summary', 'key',  'id', 'updated']

    # Initial values
    max_result = 100
    start_at = 0
    diff = 100
    counter = 0

    # Creating the csv file as iterating through issues
    with open('list.csv', mode='w') as report:
        list_writer = csv.writer(
            report, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        # Number of columns must be equal to the number of fields returned
        list_writer.writerow(
            ['Key', 'Child Issues', 'All Done', 'Last Updated'])

        while diff > 0:
            json_query = {
                'jql': jql,
                'startAt': start_at,
                'maxResults': max_result,
                'fields': fields
            }

            # API call
            response = api_call.request(
                'POST', instance + '/rest/api/3/search', json_data=json_query)
            response_json = response.json()

            for issue in response_json['issues']:
                response_epic_count = api_call.request(
                    'GET', instance + '/rest/agile/1.0/epic/'+issue['key']+'/issue')
                response_epic_json = response_epic_count.json()
                total = response_epic_json['total']

                issues = response_epic_json['issues']
                count_done = 0
                count_done_text = 'NO'

                for item in issues:
                    status_category = item['fields']['status']['statusCategory']['id']

                    if status_category == 3:
                        count_done = count_done + 1
                    
                    if count_done == total:
                        count_done_text = 'YES'

                list_writer.writerow(
                    [issue['key'], str(total), count_done_text, issue['fields']['updated']])

                print(issue['key'] + ', total: ' + str(total) + ' all done: ' + count_done_text + ' last updated: ' + issue['fields']['updated'])

            counter += 1

            start_at = counter * max_result
            diff = response_json['total'] - start_at


if __name__ == '__main__':
    main()
